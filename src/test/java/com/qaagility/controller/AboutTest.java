package com.qaagility.controller;

import org.junit.Test;
import static org.junit.Assert.*;

public class AboutTest {

    @Test
    public void testDescReturnsCorrectString() {
        // Arrange
        About about = new About();
        
        // Act
        String result = about.desc();
        
        // Assert
        assertEquals("This application was copied from somewhere, sorry but I cannot give the details and the proper copyright notice. Please don't tell the police!", result);
    }
}